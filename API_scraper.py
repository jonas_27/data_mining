"""
API scraper. Queries data from covid19 and world bank APIs.
"""
import logging
import requests
import json

import config as cfg


def covid19_api():
    """
    Queries the covid19 API and uses json to convert to dictionary.
    :return: The queried data from the API as type dict
    """
    response_covid19 = requests.get(cfg.COVID19_API)
    if not response_covid19.status_code == cfg.CODE_OK:
        raise requests.exceptions.ConnectionError("Failed to get 200 code from covid19 API")
    else:
        logging.debug(f'Success! Querying covid19 API')
        try:
            covid19_dict = json.loads(response_covid19.text)
        except ValueError:
            logging.exception('Decoding JSON has failed.')
        except Exception as e:
            logging.exception(e)
        else:
            return covid19_dict


def world_bank_api(iso6133_code):
    """
    Queries the world bank API and uses json to convert to dictionary
    :param iso6133_code: The two letter country iso6133 code (not the same as the normal iso code)
    :return: the income level per capita for a country in string type
    """
    query_url = cfg.WORLDBANK_API_BASE + iso6133_code + '?format=json'
    response_worldbank = requests.get(query_url)
    if not response_worldbank.status_code == cfg.CODE_OK:
        raise requests.exceptions.ConnectionError("Failed to get 200 code from world bank API")
    else:
        logging.debug(f'Success! Querying world bank API')
        try:
            worldbank_dict = json.loads(response_worldbank.text)
        except ValueError:
            logging.exception('Decoding JSON has failed.')
        except Exception as e:
            logging.exception(e)
        else:
            return worldbank_dict[1][0]['incomeLevel']['value']
