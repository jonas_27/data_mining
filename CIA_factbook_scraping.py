"""
The main web scraping script, which includes the main framework for the actual web scraping, including
a function for parsing a url, a function for retrieving all of the country urls from the site,
a function that scrapes each country url for data, and a function to send the output data to a SQL database.
"""

# url to Bitbucket repository: https://bitbucket.org/jonas_27/data_mining.git
import requests
from bs4 import BeautifulSoup
import sys
import logging
# attached py files import
import config as cfg
import page_handling as ph
import web_scraping_database as web_db
import API_scraper as api


def get_and_parse_webpage(my_url):
    """
    :param my_url: Gets a web_page url and parses it using BeautifulSoup
    :return: The parsed content of the web_page
    """
    web_page = requests.get(my_url)
    if not web_page.status_code == cfg.CODE_OK:
        raise requests.exceptions.ConnectionError("Failed to open url")
    else:
        logging.debug(f'Success! Scraping web_page: {my_url} \n')
        soup = BeautifulSoup(web_page.text, 'html.parser')
        return soup


def get_country_links(page):
    """
    :param page: Receives the parsed page for all of the countries and retrieves the links for each country webpage
    :return: A list of all of the country links.
    """
    country_href = []
    try:
        # Attaining the urls for each country and appending to a list
        for country in page.find_all('a', class_='flag-btn', href=True):
            country_href.append(country.get('href'))
    except AttributeError as e:
        logging.error(e)
    # Creating a list of complete urls of the countries
    country_href = country_href[1::2]
    # Creating list of keys made up of the iso-codes of each country for the
    # country urls so that we can use them as indices later on in the database
    keys = [href.split('/')[-1].split('.')[0] for href in country_href]
    # Creating a list of full urls
    country_full_urls = [cfg.COUNTRY_URL_PREFIX + c.split('/')[-1] for c in country_href]
    # Creating a dictionary where the keys are the iso-codes and the values are the respective country url.
    country_urls_dict = {keys[index]: country_full_urls[index] for index in range(len(keys))}

    return country_urls_dict


def country_data(country_url):
    """
    Collects data on a country webpage
    :param country_url: a link to the webpage of the country
    :return: a collection of data scraped from said page.
    """
    # Obtaining the parsed data from the country url
    source = get_and_parse_webpage(country_url)
    # country name
    country_name = source.find('span', class_="region").text
    # the largest common tag for society matters
    society = source.find(id='people-and-society-category-section')
    # the biggest common tag for general info
    general = source.find(id='wfb-text-holder')
    # the biggest common tag for geography info
    geography = source.find(id='geography-category-section')

    # A list containing all the country's data
    data_list = [
        country_name, ph.population_size(society),
        ph.age_structure(society, 0), ph.age_structure(society, 1),
        ph.age_structure(society, 2), ph.age_structure(society, 3), ph.age_structure(society, 4),
        ph.population_growth(society), ph.urbanization(society),
        ph.life_span(society, 0), ph.life_span(society, 1), ph.life_span(society, 2),
        ph.land_area(geography), ph.update_date(general),
        ph.coastline(geography), ph.coordinates(geography, 0), ph.coordinates(geography, 1),
        ph.natural_resources(geography), ph.climate(geography)
    ]
    # creating dictionary from cfg.titles and data_list
    data_dict = {cfg.TITLES[i]: data_list[i] for i in range(len(cfg.TITLES))}

    return data_dict


def write_to_database(countries_urls, api_dict):
    """
    Scrapes data for countries dictionary and updates the output to the database.
    :param countries_urls: dictionary of countries urls
    :param api_dict: dictionary of queried data from covid19 API
    """

    # Obtaining the data per country.
    for iso_code, url in countries_urls.items():
        logging.debug(f'Attempting to scrape and update records for: {cfg.ISO_CODE_DICT[iso_code]} ...')
        # Obtaining the scraped data for each country
        country_params = country_data(url)
        # Sending the country index and parameters to be written in the database
        web_db.writing_all_into_database(iso_code, country_params, api_dict)
        print(f'Success! Records updated for country: {cfg.ISO_CODE_DICT[iso_code]}')
        logging.info(f'Success! Records updated for country: {cfg.ISO_CODE_DICT[iso_code]}')


def main():
    """The main function. From here functions run sequentially to scrape the data from the CIA fact-book website,
    as well as the covid19 API."""
    try:
        # create or update database
        web_db.create_use_database()
        # Retrieving parsed web main country list page
        page = get_and_parse_webpage(cfg.URL_SCRAPING)
        # Getting dictionary of country links. keys are iso_codes
        country_links_dict = get_country_links(page)
        # Get covid19 data from api in form of dictionary
        covid19_api_dict = api.covid19_api()
        # Sending the dictionary of country urls to be scraped for data and then saved in the database
        write_to_database(country_links_dict, covid19_api_dict)
        # close database
        web_db.finish()
        print("\nWeb scraping process completed successfully!")
        logging.info("Web scraping process completed successfully!")
    except Exception as e:
        print(e)
        logging.error(e)
        sys.exit()


if __name__ == '__main__':
    main()
