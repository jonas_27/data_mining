"""
A command line interface for the scraper.
Enables to scrape and update all of the countries at once, or do it by country,
or do it by first letter of country which would update all countries beginning with that letter,
or updating just one child branch of the parent table "countries" (geography+resources, society+age_groups).
"""
import logging
import argparse

import CIA_factbook_scraping as cia
import config as cfg
import web_scraping_database as web_db
import API_scraper as api

# Adding logfile to program
logging.basicConfig(level=logging.DEBUG, filename='cia_factbook_log.log', filemode='w',
                    format='%(asctime)s: %(filename)s: %(levelname)s: %(message)s')


def select_all_countries():
    """scrapes the data of all of the countries to the dataset"""
    try:
        print(f'Attempting to scrape and update records for all countries ...')
        logging.info(f'Attempting to scrape and update records for all countries ...')
        cia.main()
    except Exception as e:
        logging.warning(e)
        print(e)


def select_country(country):
    """
    Scrapes the data of the chosen country
    :param country: Two letter iso code of the country.
    """
    print(f'Chosen country is: {cfg.ISO_CODE_DICT[country].capitalize()}')
    logging.info(f'Chosen country is: {cfg.ISO_CODE_DICT[country].capitalize()}')

    # Creating database if doesn't exist, else calls it
    web_db.create_use_database()
    try:
        # Getting dictionary of urls
        page = cia.get_and_parse_webpage(cfg.URL_SCRAPING)
        dict_of_countries_urls = cia.get_country_links(page)
        # Get url for iso-code that was chosen
        desired_country_link = dict_of_countries_urls[country]
        # Get covid19 data from api in form of dictionary
        covid19_api_dict = api.covid19_api()

        # sending country - url pair to scrape data and save in the database
        cia.write_to_database({country: desired_country_link}, covid19_api_dict)
        # Close database connector
        web_db.finish()
        # print(f'Success! Records updated for country: {cfg.ISO_CODE_DICT[country]}')
        # logging.info(f'Success! Records updated for country: {cfg.ISO_CODE_DICT[country]}')
    except Exception as e:
        logging.warning(e)
        print(e)


def first_letter_countries(first_letter):
    """
    Scrapes the countries starting with the chosen letter
    :param first_letter: First letter of countries
    """
    try:
        fl_countries = ", ".join([val for key, val in cfg.ISO_CODE_DICT.items()
                                  if val.startswith(first_letter.upper())])
        print(f'All countries beginning with the letter "{first_letter.upper()}" are: {fl_countries}')
        logging.info(f'All countries beginning with the letter "{first_letter.upper()}" are: {fl_countries}')
        # we create a list with the countries to scrape
        countries_to_scrape = [key for key, val in cfg.ISO_CODE_DICT.items() if val.startswith((first_letter.upper()))]

        # Creating database if doesn't exist, else calls it
        web_db.create_use_database()
        # Get the page
        page = cia.get_and_parse_webpage(cfg.URL_SCRAPING)
        # Create the dictionary to scrape from. {country: url}
        countries_urls_to_scrape = {key: cia.get_country_links(page)[key] for key in countries_to_scrape}
        # Get covid19 data from api in form of dictionary
        covid19_api_dict = api.covid19_api()
        # We now start writing into the DB
        cia.write_to_database(countries_urls_to_scrape, covid19_api_dict)
        # close connector
        web_db.finish()
        print(f'Scraping from all countries beginning with the letter "{first_letter.upper()}" finished successfully!')
        logging.info(f'Scraping from all countries beginning with the letter "{first_letter.upper()}" '
                     f'finished successfully!')
    except Exception as e:
        logging.warning(e)
        print(e)


def update_society_or_geography(table, country):
    """
        Updates only the geography table in the database for a specific country
    :param country: The two letter iso code for chosen country
    :param table: either geography or society
    """
    try:
        print(f'\nAttempting to scrape and update {table.upper()} records for: {cfg.ISO_CODE_DICT[country]} ...\n')
        logging.info(f'\nAttempting to scrape and update {table.upper()} records for:'
                     f' {cfg.ISO_CODE_DICT[country]} ...\n')
        # Creating database if doesn't exist, else calls it.
        # update_bool = False if the tables don't exist and have to be created, and True if the tables already exist.
        update_bool = web_db.create_use_database()
        # Getting dictionary of urls
        page = cia.get_and_parse_webpage(cfg.URL_SCRAPING)
        # Getting desired country url from dictionary
        country_link = cia.get_country_links(page)[country]
        # Scraping the data for the specific country
        country_params = cia.country_data(country_link)

        if update_bool:  # If the table already exists (update_bool = True), then:
            # If country record exists in child table, or if country record exists in parent table:
            if web_db.delete_old_record(table, country) or web_db.check_countries_table(country):
                # Save into society or geography table based on table variable value
                if table == 'society':
                    web_db.society_table(country, country_params)
                elif table == 'geography':
                    web_db.geography_table(country, country_params)
            # If country record doesn't exist create country record and:
            else:
                logging.debug(f"Record does not exist. Creating new record for country: {cfg.ISO_CODE_DICT[country]}")
                web_db.country_table(country, country_params)
                # Save into society or geography table based on table variable value
                if table == 'society':
                    web_db.society_table(country, country_params)
                elif table == 'geography':
                    web_db.geography_table(country, country_params)

        else:  # If table doesn't exist create country table and:
            web_db.country_table(country, country_params)
            # Save into society or geography table based on table variable value
            if table == 'society':
                web_db.society_table(country, country_params)
            elif table == 'geography':
                web_db.geography_table(country, country_params)

        # Closing the database through the connector.
        web_db.finish()
        print(f"Success: {table} table updated for country: {cfg.ISO_CODE_DICT[country]}")
        logging.info(f"Success: {table} table updated for country: {cfg.ISO_CODE_DICT[country]}")
    except Exception as e:
        logging.warning(e)
        print(e)


def parsing():
    """Parsing the arguments from the command line"""

    parser = argparse.ArgumentParser(description='Command line interface for web scraper.')

    # The user can only choose one of the 3 following options: ( either -a or -c or fl)
    group = parser.add_mutually_exclusive_group()
    # scrape all countries
    group.add_argument('-a', '--all', action='store_true', help='scrape all countries into database.')
    # scrape a specify country
    group.add_argument('-c', '--country_code', type=str,
                       help='Scrape data from a specify country. Use country iso code (2 letters).')
    # scrape countries starting with a defined letter
    group.add_argument('-fl', '--first_letter', type=str,
                       help='Scrapes data for all countries beginning with chosen letter.')

    # If the user selected one country, can also select either Geography or Society
    group_gs = parser.add_mutually_exclusive_group()
    group_gs.add_argument('-g', '--geography', action='store_true',
                          help='Will update only the geography & resources'
                               ' tables in the database per individual country')
    # Save only society related scraped data for one country into database.
    group_gs.add_argument('-s', '--society', action='store_true',
                          help='Will update only the society and age_groups tables'
                               ' in the database per individual country')

    return parser.parse_args()


def handling_parser():
    """ With conditions we make sure our input is conform and then redirect to the right function """

    args = parsing()

    # Input is 'all' (-a)
    if args.all:
        if not args.society and not args.geography:
            select_all_countries()
        else:
            logging.warning("Sorry -g and -s are only available with one country at a time [-c]")
            print("Sorry -g and -s are only available with one country input [-c]")

    # Input is 'country_code':(-c)
    if args.country_code:
        if args.country_code in cfg.ISO_CODE_DICT.keys():  # Check that the input country exists in our dict
            if not args.geography and not args.society:  # means input country is exclusive
                select_country(args.country_code)
            elif args.geography or args.society:  # input country and geography or society
                if args.geography:  # will only scrape geography data from the chosen country
                    update_society_or_geography('geography', args.country_code)
                else:  # will only scrape society data from the chosen country
                    update_society_or_geography('society', args.country_code)
        else:  # incorrect input
            logging.warning(f"Sorry your input '{args.country_code}' isn't correct for -c.\
            \nPlease enter a country in the following list {list(cfg.ISO_CODE_DICT.keys())}")
            print(f"Sorry your input '{args.country_code}' isn't correct for -c.\
            \nPlease enter a country in the following list {list(cfg.ISO_CODE_DICT.keys())}")

    # Input is First letter (-fl)
    if args.first_letter:
        if len(args.first_letter) == 1 and args.first_letter.isalpha():  # The input is conform
            if not args.society and not args.geography:
                first_letter_countries(args.first_letter)
            else:
                logging.warning("Sorry -g and -s are only available with one country input [-c]")
                print("Sorry -g and -s are only available with one country input [-c]")
        else:  # The input isn't correct
            logging.warning(f"Sorry your input '{args.first_letter}' isn't correct. Only one letter needed after -fl")
            print(f"Sorry your input '{args.first_letter}' isn't correct. Only one letter needed after -fl")


if __name__ == '__main__':
    handling_parser()
