"""configuration script holding all constants that are in use throughout the program"""

# User and password to connect to the database.
DB_USERNAME = 'root'
DB_PASSWORD = '123456'

# covid19 API URL
COVID19_API = 'https://api.covid19api.com/summary'

# world bank API URL
WORLDBANK_API_BASE = 'http://api.worldbank.org/v2/country/'

# url prefix for country webpage scrapping
COUNTRY_URL_PREFIX = 'https://www.cia.gov/library/publications/the-world-factbook/geos/'

# Country - iso code dictionary (from the cia factbook website. Country names edited to those of COUNTRY_ISO6133_DICT)
ISO_CODE_DICT = {'af': 'Afghanistan', 'ax': 'Akrotiri', 'al': 'Albania', 'ag': 'Algeria', 'aq': 'American Samoa',
                 'an': 'Andorra', 'ao': 'Angola', 'av': 'Anguilla', 'ac': 'Antigua and Barbuda', 'ar': 'Argentina',
                 'am': 'Armenia', 'aa': 'Aruba', 'at': 'Ashmore and Cartier Islands', 'as': 'Australia',
                 'au': 'Austria', 'aj': 'Azerbaijan', 'bf': 'Bahamas', 'ba': 'Bahrain', 'bg': 'Bangladesh',
                 'bb': 'Barbados', 'bo': 'Belarus', 'be': 'Belgium', 'bh': 'Belize', 'bn': 'Benin', 'bd': 'Bermuda',
                 'bt': 'Bhutan', 'bl': 'Bolivia', 'bk': 'Bosnia and Herzegovina', 'bc': 'Botswana',
                 'bv': 'Bouvet Island', 'br': 'Brazil', 'io': 'British Indian Ocean Territory',
                 'vi': 'British Virgin Islands', 'bx': 'Brunei Darussalam', 'bu': 'Bulgaria', 'uv': 'Burkina Faso',
                 'bm': 'Myanmar', 'by': 'Burundi', 'cv': 'Cabo Verde', 'cb': 'Cambodia', 'cm': 'Cameroon', 'ca': 'Canada',
                 'cj': 'Cayman Islands', 'ct': 'Central African Republic', 'cd': 'Chad', 'ci': 'Chile', 'ch': 'China',
                 'kt': 'Christmas Island', 'ip': 'Clipperton Island', 'ck': 'Cocos (Keeling) Islands', 'co': 'Colombia',
                 'cn': 'Comoros', 'cg': 'Congo (Brazzaville)', 'cf': 'Congo (Kinshasa)',
                 'cw': 'Cook Islands', 'cr': 'Coral Sea Islands', 'cs': 'Costa Rica', 'iv': "Côte d'Ivoire",
                 'hr': 'Croatia', 'cu': 'Cuba', 'uc': 'Curacao', 'cy': 'Cyprus', 'ez': 'Czech Republic',
                 'da': 'Denmark', 'dx': 'Dhekelia', 'dj': 'Djibouti', 'do': 'Dominica', 'dr': 'Dominican Republic',
                 'ec': 'Ecuador', 'eg': 'Egypt', 'es': 'El Salvador', 'ek': 'Equatorial Guinea', 'er': 'Eritrea',
                 'en': 'Estonia', 'wz': 'Swaziland', 'et': 'Ethiopia', 'ee': 'European Union',
                 'fk': 'Falkland Islands (Malvinas)', 'fo': 'Faroe Islands', 'fj': 'Fiji', 'fi': 'Finland',
                 'fr': 'France', 'fp': 'French Polynesia', 'fs': 'French Southern and Antarctic Lands', 'gb': 'Gabon',
                 'ga': 'Gambia', 'gg': 'Georgia', 'gm': 'Germany', 'gh': 'Ghana', 'gi': 'Gibraltar', 'gr': 'Greece',
                 'gl': 'Greenland', 'gj': 'Grenada', 'gq': 'Guam', 'gt': 'Guatemala', 'gk': 'Guernsey', 'gv': 'Guinea',
                 'pu': 'Guinea-Bissau', 'gy': 'Guyana', 'ha': 'Haiti', 'hm': 'Heard and Mcdonald Islands',
                 'vt': 'Holy See (Vatican City State)', 'ho': 'Honduras', 'hk': 'Hong Kong, SAR China', 'hu': 'Hungary',
                 'ic': 'Iceland', 'in': 'India', 'id': 'Indonesia', 'ir': 'Iran, Islamic Republic of', 'iz': 'Iraq',
                 'ei': 'Ireland', 'im': 'Isle of Man', 'is': 'Israel', 'it': 'Italy', 'jm': 'Jamaica',
                 'jn': 'Jan Mayen', 'ja': 'Japan', 'je': 'Jersey', 'jo': 'Jordan', 'kz': 'Kazakhstan', 'ke': 'Kenya',
                 'kr': 'Kiribati', 'kn': 'Korea (North)', 'ks': 'Korea (South)', 'kv': 'Kosovo', 'ku': 'Kuwait',
                 'kg': 'Kyrgyzstan', 'la': 'Lao PDR', 'lg': 'Latvia', 'le': 'Lebanon', 'lt': 'Lesotho', 'li': 'Liberia',
                 'ly': 'Libya', 'ls': 'Liechtenstein', 'lh': 'Lithuania', 'lu': 'Luxembourg', 'mc': 'Macao, SAR China',
                 'ma': 'Madagascar', 'mi': 'Malawi', 'my': 'Malaysia', 'mv': 'Maldives', 'ml': 'Mali', 'mt': 'Malta',
                 'rm': 'Marshall Islands', 'mr': 'Mauritania', 'mp': 'Mauritius', 'mx': 'Mexico',
                 'fm': 'Micronesia, Federated States of', 'md': 'Moldova', 'mn': 'Monaco', 'mg': 'Mongolia',
                 'mj': 'Montenegro', 'mh': 'Montserrat', 'mo': 'Morocco', 'mz': 'Mozambique', 'wa': 'Namibia',
                 'nr': 'Nauru', 'bq': 'Navassa Island', 'np': 'Nepal', 'nl': 'Netherlands', 'nc': 'New Caledonia',
                 'nz': 'New Zealand', 'nu': 'Nicaragua', 'ng': 'Niger', 'ni': 'Nigeria', 'ne': 'Niue',
                 'nf': 'Norfolk Island', 'mk': 'Macedonia', 'cq': 'Northern Mariana Islands', 'no': 'Norway',
                 'mu': 'Oman', 'pk': 'Pakistan', 'ps': 'Palau', 'pm': 'Panama', 'pp': 'Papua New Guinea',
                 'pa': 'Paraguay', 'pe': 'Peru', 'rp': 'Philippines', 'pc': 'Pitcairn', 'pl': 'Poland',
                 'po': 'Portugal', 'rq': 'Puerto Rico', 'qa': 'Qatar', 'ro': 'Romania', 'rs': 'Russian Federation',
                 'rw': 'Rwanda', 'tb': 'Saint-Barthélemy', 'sh': 'Saint Helena', 'sc': 'Saint Kitts and Nevis',
                 'st': 'Saint Lucia', 'rn': 'Saint-Martin (French part)', 'sb': 'Saint Pierre and Miquelon',
                 'vc': 'Saint Vincent and Grenadines', 'ws': 'Samoa', 'sm': 'San Marino',
                 'tp': 'Sao Tome and Principe', 'sa': 'Saudi Arabia', 'sg': 'Senegal', 'ri': 'Serbia',
                 'se': 'Seychelles', 'sl': 'Sierra Leone', 'sn': 'Singapore', 'nn': 'Sint Maarten', 'lo': 'Slovakia',
                 'si': 'Slovenia', 'bp': 'Solomon Islands', 'so': 'Somalia', 'sf': 'South Africa',
                 'sx': 'South Georgia and the South Sandwich Islands', 'od': 'South Sudan', 'sp': 'Spain',
                 'ce': 'Sri Lanka', 'su': 'Sudan', 'ns': 'Suriname', 'sv': 'Svalbard and Jan Mayen Islands',
                 'sw': 'Sweden', 'sz': 'Switzerland', 'sy': 'Syrian Arab Republic (Syria)',
                 'tw': 'Taiwan, Republic of China', 'ti': 'Tajikistan', 'tz': 'Tanzania, United Republic of',
                 'th': 'Thailand', 'tt': 'Timor-Leste', 'to': 'Togo', 'tl': 'Tokelau', 'tn': 'Tonga',
                 'td': 'Trinidad and Tobago', 'ts': 'Tunisia', 'tu': 'Turkey', 'tx': 'Turkmenistan',
                 'tk': 'Turks and Caicos Islands', 'tv': 'Tuvalu', 'ug': 'Uganda', 'up': 'Ukraine',
                 'ae': 'United Arab Emirates', 'uk': 'United Kingdom', 'us': 'United States of America',
                 'um': 'United States Pacific Island Wildlife Refuges', 'uy': 'Uruguay', 'uz': 'Uzbekistan',
                 'nh': 'Vanuatu', 've': 'Venezuela (Bolivarian Republic)', 'vm': 'Viet Nam', 'vq': 'Virgin Islands, US',
                 'wq': 'Wake Island', 'wf': 'Wallis and Futuna Islands', 'ym': 'Yemen', 'za': 'Zambia',
                 'zi': 'Zimbabwe'}

# country iso3166-1 dictionary (from the covid19 API)
COUNTRY_ISO3166_DICT = {'ALA Aland Islands': 'ax', 'Afghanistan': 'af', 'Albania': 'al', 'Algeria': 'dz',
                        'American Samoa': 'as', 'Andorra': 'ad', 'Angola': 'ao', 'Anguilla': 'ai', 'Antarctica': 'aq',
                        'Antigua and Barbuda': 'ag', 'Argentina': 'ar', 'Armenia': 'am', 'Aruba': 'aw',
                        'Australia': 'au', 'Austria': 'at', 'Azerbaijan': 'az', 'Bahamas': 'bs', 'Bahrain': 'bh',
                        'Bangladesh': 'bd', 'Barbados': 'bb', 'Belarus': 'by', 'Belgium': 'be', 'Belize': 'bz',
                        'Benin': 'bj', 'Bermuda': 'bm', 'Bhutan': 'bt', 'Bolivia': 'bo', 'Bosnia and Herzegovina': 'ba',
                        'Botswana': 'bw', 'Bouvet Island': 'bv', 'Brazil': 'br', 'British Indian Ocean Territory': 'io',
                        'British Virgin Islands': 'vg', 'Brunei Darussalam': 'bn', 'Bulgaria': 'bg',
                        'Burkina Faso': 'bf', 'Burundi': 'bi', 'Cambodia': 'kh', 'Canada': 'ca', 'Cape Verde': 'cv',
                        'Cayman Islands': 'ky', 'Central African Republic': 'cf', 'Chad': 'td', 'Chile': 'cl',
                        'China': 'cn', 'Christmas Island': 'cx', 'Cocos (Keeling) Islands': 'cc', 'Colombia': 'co',
                        'Comoros': 'km', 'Congo (Brazzaville)': 'cg', 'Congo (Kinshasa)': 'cd', 'Cook Islands': 'ck',
                        'Costa Rica': 'cr', 'Croatia': 'hr', 'Cuba': 'cu', 'Cyprus': 'cy', 'Czech Republic': 'cz',
                        "Côte d'Ivoire": 'ci', 'Denmark': 'dk', 'Djibouti': 'dj', 'Dominica': 'dm',
                        'Dominican Republic': 'do', 'Ecuador': 'ec', 'Egypt': 'eg', 'El Salvador': 'sv',
                        'Equatorial Guinea': 'gq', 'Eritrea': 'er', 'Estonia': 'ee', 'Ethiopia': 'et',
                        'Falkland Islands (Malvinas)': 'fk', 'Faroe Islands': 'fo', 'Fiji': 'fj', 'Finland': 'fi',
                        'France': 'fr', 'French Guiana': 'gf', 'French Polynesia': 'pf',
                        'French Southern Territories': 'tf', 'Gabon': 'ga', 'Gambia': 'gm', 'Georgia': 'ge',
                        'Germany': 'de', 'Ghana': 'gh', 'Gibraltar': 'gi', 'Greece': 'gr', 'Greenland': 'gl',
                        'Grenada': 'gd', 'Guadeloupe': 'gp', 'Guam': 'gu', 'Guatemala': 'gt', 'Guernsey': 'gg',
                        'Guinea': 'gn', 'Guinea-Bissau': 'gw', 'Guyana': 'gy', 'Haiti': 'ht',
                        'Heard and Mcdonald Islands': 'hm', 'Holy See (Vatican City State)': 'va', 'Honduras': 'hn',
                        'Hong Kong, SAR China': 'hk', 'Hungary': 'hu', 'Iceland': 'is', 'India': 'in',
                        'Indonesia': 'id', 'Iran, Islamic Republic of': 'ir', 'Iraq': 'iq', 'Ireland': 'ie',
                        'Isle of Man': 'im', 'Israel': 'il', 'Italy': 'it', 'Jamaica': 'jm', 'Japan': 'jp',
                        'Jersey': 'je', 'Jordan': 'jo', 'Kazakhstan': 'kz', 'Kenya': 'ke', 'Kiribati': 'ki',
                        'Korea (North)': 'kp', 'Korea (South)': 'kr', 'Kuwait': 'kw', 'Kyrgyzstan': 'kg',
                        'Lao PDR': 'la', 'Latvia': 'lv', 'Lebanon': 'lb', 'Lesotho': 'ls', 'Liberia': 'lr',
                        'Libya': 'ly', 'Liechtenstein': 'li', 'Lithuania': 'lt', 'Luxembourg': 'lu',
                        'Macao, SAR China': 'mo', 'Macedonia': 'mk', 'Madagascar': 'mg', 'Malawi': 'mw',
                        'Malaysia': 'my', 'Maldives': 'mv', 'Mali': 'ml', 'Malta': 'mt', 'Marshall Islands': 'mh',
                        'Martinique': 'mq', 'Mauritania': 'mr', 'Mauritius': 'mu', 'Mayotte': 'yt', 'Mexico': 'mx',
                        'Micronesia, Federated States of': 'fm', 'Moldova': 'md', 'Monaco': 'mc', 'Mongolia': 'mn',
                        'Montenegro': 'me', 'Montserrat': 'ms', 'Morocco': 'ma', 'Mozambique': 'mz', 'Myanmar': 'mm',
                        'Namibia': 'na', 'Nauru': 'nr', 'Nepal': 'np', 'Netherlands': 'nl',
                        'Netherlands Antilles': 'an', 'New Caledonia': 'nc', 'New Zealand': 'nz', 'Nicaragua': 'ni',
                        'Niger': 'ne', 'Nigeria': 'ng', 'Niue': 'nu', 'Norfolk Island': 'nf',
                        'Northern Mariana Islands': 'mp', 'Norway': 'no', 'Oman': 'om', 'Pakistan': 'pk', 'Palau': 'pw',
                        'Palestinian Territory': 'ps', 'Panama': 'pa', 'Papua New Guinea': 'pg', 'Paraguay': 'py',
                        'Peru': 'pe', 'Philippines': 'ph', 'Pitcairn': 'pn', 'Poland': 'pl', 'Portugal': 'pt',
                        'Puerto Rico': 'pr', 'Qatar': 'qa', 'Republic of Kosovo': 'xk', 'Romania': 'ro',
                        'Russian Federation': 'ru', 'Rwanda': 'rw', 'Réunion': 're', 'Saint Helena': 'sh',
                        'Saint Kitts and Nevis': 'kn', 'Saint Lucia': 'lc', 'Saint Pierre and Miquelon': 'pm',
                        'Saint Vincent and Grenadines': 'vc', 'Saint-Barthélemy': 'bl',
                        'Saint-Martin (French part)': 'mf', 'Samoa': 'ws', 'San Marino': 'sm',
                        'Sao Tome and Principe': 'st', 'Saudi Arabia': 'sa', 'Senegal': 'sn', 'Serbia': 'rs',
                        'Seychelles': 'sc', 'Sierra Leone': 'sl', 'Singapore': 'sg', 'Slovakia': 'sk', 'Slovenia': 'si',
                        'Solomon Islands': 'sb', 'Somalia': 'so', 'South Africa': 'za',
                        'South Georgia and the South Sandwich Islands': 'gs', 'South Sudan': 'ss', 'Spain': 'es',
                        'Sri Lanka': 'lk', 'Sudan': 'sd', 'Suriname': 'sr', 'Svalbard and Jan Mayen Islands': 'sj',
                        'Swaziland': 'sz', 'Sweden': 'se', 'Switzerland': 'ch', 'Syrian Arab Republic (Syria)': 'sy',
                        'Taiwan, Republic of China': 'tw', 'Tajikistan': 'tj', 'Tanzania, United Republic of': 'tz',
                        'Thailand': 'th', 'Timor-Leste': 'tl', 'Togo': 'tg', 'Tokelau': 'tk', 'Tonga': 'to',
                        'Trinidad and Tobago': 'tt', 'Tunisia': 'tn', 'Turkey': 'tr', 'Turkmenistan': 'tm',
                        'Turks and Caicos Islands': 'tc', 'Tuvalu': 'tv', 'US Minor Outlying Islands': 'um',
                        'Uganda': 'ug', 'Ukraine': 'ua', 'United Arab Emirates': 'ae', 'United Kingdom': 'gb',
                        'United States of America': 'us', 'Uruguay': 'uy', 'Uzbekistan': 'uz', 'Vanuatu': 'vu',
                        'Venezuela (Bolivarian Republic)': 've', 'Viet Nam': 'vn', 'Virgin Islands, US': 'vi',
                        'Wallis and Futuna Islands': 'wf', 'Western Sahara': 'eh', 'Yemen': 'ye', 'Zambia': 'zm',
                        'Zimbabwe': 'zw'}

# Dict for months values
MONTHS = dict(January='1', February='2', March='3', April='5', May='6', June='7', August='8', September='9',
              October='10', November='11', December='12')

# If web page ok: returns 200
CODE_OK = 200

# flags of the world CIA factbook url
URL_SCRAPING = 'https://www.cia.gov/library/publications/the-world-factbook/docs/flagsoftheworld.html'

# A list of headings for the acquired data from the website
TITLES = ['Country', 'Population', '0-14', '15-24', '25-54', '55-64', 'over_65',
          'Population_Growth_%', 'Urbanization_Growth_%', 'Total_Lifespan_years', 'Male_Lifespan_years',
          'Female_Lifespan_years', 'Land_Area_sq_km', 'Latest_Updated_Data', 'Coastline', 'Latitude', 'Longitude',
          'Natural_Resources', 'climate']

# the database name and tables for the web_scraping_database script
DB_FILENAME = 'CIA_factbook'

# Database tables
TABLES = {}
TABLES['countries'] = ('''CREATE TABLE `countries` (
                        `iso_code` VARCHAR(2) NOT NULL,
                        `country_name` VARCHAR(50) NOT NULL,
                        `iso6133_code` VARCHAR(2) NULL,
                        `last_website_update` DATE DEFAULT NULL,
                        `last_update_request` VARCHAR(50) DEFAULT NULL,
                        PRIMARY KEY (`iso_code`)
                        )''')
TABLES['society'] = ('''CREATE TABLE `society` (
                        `id` int NOT NULL AUTO_INCREMENT,
                        `total_population` INT NOT NULL,
                        `population_growth` FLOAT NOT NULL,
                        `urbanization_growth` FLOAT NOT NULL,
                        `total_lifespan` FLOAT NOT NULL,
                        `male_lifespan` FLOAT NOT NULL,
                        `female_lifespan` FLOAT NOT NULL,
                        `country_iso` VARCHAR(2) NULL,
                        `last_update_request` VARCHAR(50) DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        FOREIGN KEY (`country_iso`) REFERENCES countries(`iso_code`) ON UPDATE CASCADE ON DELETE CASCADE
                        )''')
TABLES['geography'] = ('''CREATE TABLE `geography` (
                        `id` int NOT NULL AUTO_INCREMENT,
                        `area_sq_km` FLOAT NOT NULL,
                        `Coastline` FLOAT NULL, 
                         `Latitude` FLOAT NULL, 
                        `Longitude` FLOAT NULL, 
                        `climate` VARCHAR(255) DEFAULT '' NOT NULL,
                        `country_iso` VARCHAR(2) DEFAULT '' NOT NULL,
                        `last_update_request` VARCHAR(50) DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        FOREIGN KEY (`country_iso`) REFERENCES countries(`iso_code`) ON UPDATE CASCADE ON DELETE CASCADE
                        )''')
TABLES['age_groups'] = ('''CREATE TABLE `age_groups` (
                        `id` int NOT NULL AUTO_INCREMENT,
                        `age_group` VARCHAR(80) NOT NULL,
                        `percent_of_population` FLOAT NOT NULL,
                        `number_of_males` INT NOT NULL,
                        `number_of_females` INT NOT NULL,
                        `population_id` INT NOT NULL,
                        `last_update_request` VARCHAR(50) DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        FOREIGN KEY (`population_id`) REFERENCES society(`id`) ON UPDATE CASCADE ON DELETE CASCADE
                        )''')
TABLES['resources'] = ('''CREATE TABLE `resources` (
                        `id` int NOT NULL AUTO_INCREMENT,
                        `metal_ores` INT NOT NULL,
                        `fossil_fuels` INT NOT NULL,
                        `non_metal_minerals` INT NOT NULL,
                        `biomass` INT NOT NULL,
                        `other` INT NOT NULL,
                        `geo_id` INT NOT NULL,
                        `last_update_request` VARCHAR(50) DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        FOREIGN KEY (`geo_id`) REFERENCES geography(`id`) ON UPDATE CASCADE ON DELETE CASCADE
                        )''')
TABLES['covid19'] = ('''CREATE TABLE `covid19` (
                        `id` int NOT NULL AUTO_INCREMENT,
                        `NewConfirmed` INT NULL,
                        `TotalConfirmed` INT NULL,
                        `NewDeaths` INT NULL,
                        `TotalDeaths` INT NULL,
                        `NewRecovered` INT NULL,
                        `TotalRecovered` INT NULL,
                        `last_api_update` DATETIME DEFAULT NULL,
                        `country_iso` VARCHAR(2) NULL,
                        `last_update_request` VARCHAR(50) DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        FOREIGN KEY (`country_iso`) REFERENCES countries(`iso_code`) ON UPDATE CASCADE ON DELETE CASCADE
                        )''')
TABLES['income_level'] = ('''CREATE TABLE `income_level` (
                        `id` int NOT NULL AUTO_INCREMENT,
                        `income_level` VARCHAR(25) NULL,
                        `country_iso` VARCHAR(2) NULL,
                        `last_update_request` VARCHAR(50) DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        FOREIGN KEY (`country_iso`) REFERENCES countries(`iso_code`) ON UPDATE CASCADE ON DELETE CASCADE
                        )''')

# Categorization into family groups of resources. These variables lists a country's mineral, petroleum, hydropower,
# and other resources of commercial importance, such as rare earth elements (REEs). In general, products appear
# only if they make a significant contribution to the economy, or are likely to do so in the future.
METALS = ['gold', 'silver', 'copper', 'zinc', 'iron', 'lead', 'nickel', 'tin', 'uranium', 'cobalt', 'bauxite',
          'tungsten', 'antimony', 'manganese', 'molybdenum', 'vanadium', 'magnetite', 'aluminum', 'selenium',
          'strontium', 'titanium', 'mercury']
ENERGY_FUELS = ['petroleum', 'coal', 'natural gas', 'hydropower']
NM_MINERALS = ['sand', 'limestone', 'clay', 'sand', ' stone', 'diamonds', 'quartz', 'asbestos', 'gypsum',
               'rare earth elements', 'phosphate', 'sulfur', 'cadmium', 'ferrosilicon', 'gallium',
               'germanium', 'hafnium', 'indium', 'lithium', 'graphite', 'gemstones', 'dolomite', 'amber']
BIO = ['fish', 'whales', 'wildlife', 'lobster', 'crab', 'krill', 'coconuts', 'squid', 'moss', 'seaweed', 'timber']

# The keys for the resources dictionary in the page handling script
RESOURCE_TITLES = ['metal_ores', 'fossil_fuels', 'non_metallic_minerals', 'biomass', 'other']

# The keys for the age_structure dictionary in the page handling script
AGE_GROUP_CATEGORIES = ['total_percent', 'num_of_males', 'num_of_females']

# regex patterns to retrieve country's info
SPLIT_PATTERN = r";|,"
FIND_AREA_PATTERN = r'(.+) sq km'
GROWTH_PATTERN = r'\n(.+)%'
AGE_STRUCTURE_PATTERN = r'\n(.+)\n(.+)%\n.male (.+).female (.+)\)'
EXPECTANCY_PATTERN = r'(\d+(?:\.\d+)?)'
COORD_PATTERN = r'(.+).\w'
URBAN_PATTERN = r'\d+'
COASTLINE_PATTERN = r'(.+) km'

# Const number to limit the climate string
MAX_CLIMATE_STR = 32


# web_scraping_database constants
TITLE_AGE_GROUPS = TITLES[2:7]
NATURAL_RESOURCES = TITLES[-2]

