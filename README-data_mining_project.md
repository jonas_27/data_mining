# Data Mining Project

A brief description of first data mining project: 
* General purpose - first hands on experience in extracting large amounts of data from the internet, aggregating, processing and analyzing the data, 
all within one program that is built from scratch.

* Specifically, the program extracts data from the cia factbook website (https://www.cia.gov/library/publications/the-world-factbook/), and two APIs, 
those of updated reports of covid19 cases from around the world, and the world bank.
The program saves the data into a designated database that is either created if not existing yet, or used if already in existance.
Different functionalities of the program can be accessed through a designated command line interface.


### Prerequisites

Running this program can either be done through Pycharm or through any computer terminal.
MySQL must also be installed on the computer.

VERY IMPORTANT: Before running the program, please access the config.py file and change DB_USERNAME and DB_PASSWORD according to your MySQL auhtentification.


### Usage

Concerning the CLI: You have several options for scraping: 
- You can choose to scrape everything [-a]
	
	
	$ python cli_interface.py -a	=> Will scrape all the countries data
 
- You can choose to scrape all countries startiong from a certain letter [-fl] + 'letter' 


	$ python cli_interface.py -fl s     => Will scrape the data from countries starting with the letter s: Spain, Serbia, Sweden,...

- You can choose to scrape a certain country only [-c] + 'country_iso_code' (2 letters) In addition to this last one [-c]: 


	$ python cli_interface.py -c is     => Will scrape the data from Israel only

- You can choose to scrape the society content only [-c] 'xx' [-s] - You can choose to scrape the geography content only [-c] 'xx' [-g]


	$ python cli_interface.py -c is -s      => Will scrape the society data from Israel only
or

	$ python cli_interface.py -c is -g      => Will scrape the geography data from Israel only


Incorrect input will return an appropriate error message


## Built With

* **Pycharm**
* **MySQL**

## Authors

* **Shai Ben David**
* **Jonas Sala**

Bitbucket repository: https://bitbucket.org/jonas_27/data_mining/src/master/

## Acknowledgments

* To Israel Tech Challenge (ITC) for all of their help

