"""The web scraping database, includes:
- create_use_database function: create or use a database and the corresponding tables.
- countries_table, society_table, geography_table, age_groups_table, resources_table, write_all_into_database:
    update the records in these tables.
- delete_old_record: delete prior records before update of record.
- check_countries_table: see if the country exists in the parent table before doing any operation on another table.
- finish: to disconnect with the database."""

import config as cfg
import API_scraper as api

import logging
from datetime import datetime
import mysql.connector
from mysql.connector import errorcode
import pandas as pd
import json


# Connecting to the database
cnx = mysql.connector.connect(user=cfg.DB_USERNAME, password=cfg.DB_PASSWORD)
cur = cnx.cursor()


def create_use_database():
    """
    Creates a database if doesn't exist. If error exists displays error on screen.
    :return: A boolean variable update_bool for the tables already exist or not.
    """
    # Pertains to the creation of database
    try:
        cur.execute(
            f"CREATE DATABASE IF NOT EXISTS {cfg.DB_FILENAME} DEFAULT CHARACTER SET 'utf8'")
    except mysql.connector.Error as err:
        logging.debug(f'Failed creating database: {err}')
        exit(1)
    # Pertains to the usage of database
    try:
        cur.execute(f"USE {cfg.DB_FILENAME}")
    except mysql.connector.Error as err:
        logging.debug(f"Database {cfg.DB_FILENAME} does not exists.")
        if err.errno == errorcode.ER_BAD_DB_ERROR:
            create_use_database(cur)
            logging.debug(f"Database {cfg.DB_FILENAME} created successfully.")
            cnx.database = cfg.DB_FILENAME
        else:
            logging.warning(err)
            exit(1)

    # Boolean variable for whether the tables exist or not.
    update_bool = None

    for table_name in cfg.TABLES:  # Creating tables
        table_description = cfg.TABLES[table_name]
        try:
            logging.debug(f"Creating table {table_name}: ")
            cur.execute(table_description)
            update_bool = False  # table doesn't exist
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                logging.debug("already exists.")
                update_bool = True  # table already exists
            else:
                logging.warning(err.msg)
        else:
            logging.debug("OK")
    return update_bool


# The parent table
def country_table(index, input_data):
    """Writes scraped data into countries table
    :param index: The two letter country code
    :param input_data: The scraped country data"""
    # Obtaining the desired variables from the input_data dictionary
    country_name = cfg.ISO_CODE_DICT[index]
    last_update = input_data["Latest_Updated_Data"]
    try:
        iso6133_code = cfg.COUNTRY_ISO3166_DICT[country_name].upper()
    except:
        pass
        iso6133_code = None
    now = datetime.now()  # Getting the current time
    country_parameters = [index, country_name, iso6133_code, last_update, now.strftime("%d/%m/%Y %H:%M:%S")]

    cur.execute(
        """REPLACE INTO countries (iso_code, country_name, iso6133_code, last_website_update, last_update_request)
         VALUES (%s, %s, %s, STR_TO_DATE(%s, '%m-%d-%Y'), %s)""",
        country_parameters)  # Executing the MySQL command.
    logging.debug("Record updated into countries table.")
    cnx.commit()


# Child table
def society_table(index, input_data):
    """Writes scraped data into population_stats
    :param index: The two letter country code
    :param input_data: The scraped country data"""
    # Obtaining the desired variables from the input_data dictionary.
    tot_pop = input_data["Population"]
    pop_growth = input_data["Population_Growth_%"]
    urban_growth = input_data["Urbanization_Growth_%"]
    tot_lifespan = input_data["Total_Lifespan_years"]
    m_lifespan = input_data["Male_Lifespan_years"]
    f_lifespan = input_data["Female_Lifespan_years"]
    now = datetime.now()  # Getting the current time
    # for now the age groups are left out since it wasn't clear how to add them in the table.
    pop_parameters = [tot_pop, pop_growth, urban_growth, tot_lifespan, m_lifespan, f_lifespan, index,
                      now.strftime("%d/%m/%Y %H:%M:%S")]

    cur.execute(
        """INSERT INTO society (total_population, population_growth, urbanization_growth, total_lifespan,
         male_lifespan, female_lifespan, country_iso, last_update_request) VALUES (%s, %s, %s, %s,%s, %s, %s, %s)""",
        pop_parameters)  # Executing the MySQL command.

    cur.execute(f'SELECT id FROM society WHERE country_iso = "{index}"')
    record_id = cur.fetchall()
    # Update records in age_groups table
    age_groups_table(record_id[0][0], input_data)
    cnx.commit()
    logging.debug("Record updated into society table.")


def age_groups_table(foreign_id, input_data_dict):
    """
    Updates the country record in the age_groups table
    :param foreign_id: The country_iso of the country
    :param input_data_dict: Dictionary of scraped data
    """
    age_group_names = cfg.TITLE_AGE_GROUPS
    for name in age_group_names:
        if input_data_dict[name] != 0:
            total_in_percent = input_data_dict[name]['total_percent']
            number_of_males = input_data_dict[name]['num_of_males']
            number_of_females = input_data_dict[name]['num_of_females']
        else:
            total_in_percent, number_of_males, number_of_females = 0, 0, 0
        now = datetime.now()
        age_group_params = [name, total_in_percent, number_of_males, number_of_females, foreign_id,
                            now.strftime("%d/%m/%Y %H:%M:%S")]
        # Executing the MySQL command.
        cur.execute("""INSERT INTO age_groups (age_group, percent_of_population, number_of_males, number_of_females,
             population_id, last_update_request) VALUES (%s, %s, %s, %s, %s, %s)""", age_group_params)
    cnx.commit()
    logging.debug("Record updated into age_groups table.")


def resources_table(foreign_id, input_data_dict):
    """
    Updates the country record in the resources table
    :param foreign_id: The country_iso of the country
    :param input_data_dict: Dictionary of scraped data
    """
    metals = input_data_dict[cfg.NATURAL_RESOURCES]['metal_ores']
    fuels = input_data_dict[cfg.NATURAL_RESOURCES]['fossil_fuels']
    minerals = input_data_dict[cfg.NATURAL_RESOURCES]['non_metallic_minerals']
    biomass = input_data_dict[cfg.NATURAL_RESOURCES]['biomass']
    other = input_data_dict[cfg.NATURAL_RESOURCES]['other']
    now = datetime.now()
    resources_params = [metals, fuels, minerals, biomass, other, foreign_id, now.strftime("%d/%m/%Y %H:%M:%S")]
    # Executing the MySQL command.
    cur.execute("""INSERT INTO resources (metal_ores, fossil_fuels, non_metal_minerals, biomass, other, geo_id,
    last_update_request) VALUES (%s, %s, %s, %s, %s, %s, %s)""", resources_params)
    cnx.commit()
    logging.debug("Record updated into resources table.")


def geography_table(index, input_data):
    """Writes scraped data into land_stats table
    :param index: The two letter country code
    :param input_data: The scraped country data"""
    # Obtaining the desired variables from the input_data dictionary.
    tot_area = input_data["Land_Area_sq_km"]
    coastline = input_data["Coastline"]
    latitude = input_data["Latitude"]
    longitude = input_data["Longitude"]
    climate = input_data['climate']
    # Getting the current time.
    now = datetime.now()
    # for now climate and resources are not included since they are too long.
    land_parameters = [tot_area, coastline, latitude, longitude, climate, index, now.strftime("%d/%m/%Y %H:%M:%S")]

    # Executing the MySQL command.
    cur.execute(
        """INSERT INTO geography (area_sq_km, Coastline, Latitude, Longitude, climate,
         country_iso, last_update_request) VALUES (%s, %s, %s, %s, %s, %s, %s);""",
        land_parameters)
    cur.execute(f'SELECT id FROM geography WHERE country_iso = "{index}"')
    record_id = cur.fetchall()
    resources_table(record_id[0][0], input_data)
    cnx.commit()
    logging.debug("Record updated into geography table.")


def covid19_table(iso_code, covid19_dict):
    """
    Writes queried data from covid19 API into covid19 table
    :param iso_code: The two letter country code
    :param covid19_dict: The queried data from the API as type dict
    """
    # Instantiating the country_covid19_data list with default values
    country_covid19_data = [None, None, None, None, None, None, None, iso_code, datetime.now()]
    # Iterating over the list of country dictionaries from the covid19 API
    for index, country_dict in enumerate(covid19_dict['Countries']):
        try:
            # Comparing country names from ISO_CODE_DICT to the country name in each country dictionary
            if cfg.COUNTRY_ISO3166_DICT[cfg.ISO_CODE_DICT[iso_code]].upper() == country_dict['CountryCode']:
                country_covid19_data = [country_dict['NewConfirmed'], country_dict['TotalConfirmed'],
                                        country_dict['NewDeaths'], country_dict['TotalDeaths'],
                                        country_dict['NewRecovered'], country_dict['TotalRecovered'],
                                        pd.to_datetime(country_dict['Date']).strftime('%Y/%m/%d %H:%S'),
                                        iso_code, datetime.now()]
        except:
            pass
    # Executing the MySQL command.
    cur.execute(
        """INSERT INTO covid19 (NewConfirmed, TotalConfirmed, NewDeaths, TotalDeaths, NewRecovered, TotalRecovered, 
        last_api_update, country_iso, last_update_request) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)""",
        country_covid19_data)
    cnx.commit()
    logging.debug("Record updated into covid19 table.")


def income_level_table(iso_code):
    """
    Calls world bank API query function "world_bank_api" for specific iso_code, and writes to income_level table
    :param iso_code: The two letter country code
    """
    # Setting default values into the income_level_parameters list for each iso_code
    income_level_parameters = [None, iso_code, datetime.now()]
    # Getting iso_code country name
    country_name = cfg.ISO_CODE_DICT[iso_code]
    for name, code in cfg.COUNTRY_ISO3166_DICT.items():
        # Looking for country name in COUNTRY_ISO3166_DICT, and retrieving said country's iso3166_code
        if country_name == name:
            iso3166_code = code
            try:
                # Sending query request for world bank API
                world_bank_dict = api.world_bank_api(iso3166_code)
                # Updating first cell in income_level_parameters
                income_level_parameters[0] = world_bank_dict
            except (IndexError, json.decoder.JSONDecodeError) as e:
                logging.error(e)
            else:
                pass
    # Executing the MySQL command.
    cur.execute(
        """INSERT INTO income_level (income_level, country_iso, last_update_request) VALUES (%s, %s, %s)""",
        income_level_parameters)
    cnx.commit()
    logging.debug("Record updated into income_level table.")


def writing_all_into_database(iso_code, country_data, api_dict):
    """
    Writes scraped data into all tables in the database
    :param iso_code: The two letter country code
    :param country_data: The scraped country data
    """
    country_table(iso_code, country_data)
    society_table(iso_code, country_data)
    geography_table(iso_code, country_data)

    covid19_table(iso_code, api_dict)
    income_level_table(iso_code)


def delete_old_record(table, country_iso):
    """
    This function will delete a record if it exists and return a boolean value of True, otherwise returns False.
    :param country_iso: The iso code for the desired country record
    :return: True if the row had to be deleted, or False if the row didn't exist.
    """
    # Getting the ID for the last record with the desired country iso code.
    cur.execute(f'SELECT id FROM {table} WHERE country_iso = "{country_iso}";')
    record_id = cur.fetchall()
    # If there is no ID, i.e there is no record, return False.
    if not record_id:
        return False
    # If record_id holds an ID number, erase the old record and return True.
    else:
        cur.execute(f'DELETE FROM {table} WHERE id ={record_id[0][0]};')
        cnx.commit()
        return True


def check_countries_table(country_iso):
    """
    This function checks to see if the country exists in the countries table (parent table)
    :param country_iso: Country iso code
    :return: A boolean value of True if a record exists in the parent table, and False if it does not exist.
    """
    cur.execute(f'SELECT iso_code FROM countries WHERE iso_code = "{country_iso}"')
    iso_exists = cur.fetchall()
    if iso_exists:
        return True
    else:
        return False


def finish():
    """Disconnecting from the database."""
    cur.close()
    cnx.close()


if __name__ == '__main__':
    pass
