"""Stores all of the functions that are related to webpage scraping"""
import config as cfg

import re
import logging


def land_area(geography):
    """
    return the Total land area of a country
    :param geography: the parsed geography tag from the country webpage
    :return: the area in sq km as type float
    """
    # field is the largest common tag for geography info
    field = geography.find(id='field-area')

    area = 0  # we initialize the area
    try:
        temp_list_area = []
        for field_area in field.find_all(class_="subfield-number"):
            new_area = re.findall(cfg.FIND_AREA_PATTERN, field_area.text)
            temp_list_area.append(new_area)
        area = temp_list_area[1]
        area = float(''.join(area[0].split(',')))
    except IndexError:
        logging.warning("Area parameter could not be scraped for this country.")

    return area


def update_date(general):
    """
    returns the date of last update made on this country
    :param general: general tag from parsed country webpage
    :return: the last updated date on the webpage
    """
    # general is the largest common tag for general info

    try:
        last_updated = general.find(class_='lastMod').text
        last_updated = last_updated.split("Page last updated on ")[1]

        # Below we get separately the day, month and year and write it all as a new string: updated_date
        temp_date = last_updated.split()
        day = temp_date[1].replace(',', '')
        month = cfg.MONTHS.get(temp_date[0])
        year = temp_date[-1]
        updated_date = month + "-" + day + "-" + year
    except Exception as e:
        logging.warning(e)

    logging.info("Last updated date scraped successfully from the country webpage.")
    return updated_date


def population_size(society):
    """
    returns the population size
    :param society: the society tag from the the parsed country webpage
    :return: the population as type int
    """
    # society is the largest common tag for society matters

    population_num = 0  # we initialize the population number

    try:
        population_tag = society.find(id="field-population")
        population = population_tag.span.text
        population_num = int(''.join(population.split(',')))
    except ValueError:
        logging.warning("Population size couldn't be scraped for this country.")

    return population_num


def population_growth(society):
    """
    returns the population growth rate in percentage
    :param society: the society tag from the parsed country webpage
    :return: the population growth rate as type float
    """

    pop_growth_percent = 0  # we initialize the population growth percentage

    try:
        pop_growth = society.find(id='field-population-growth-rate')
        pop_growth = pop_growth.div.text
        pop_growth_percent = float(re.findall(cfg.GROWTH_PATTERN, pop_growth)[0])
    except AttributeError:
        logging.warning("Population growth parameter doesn't exist for this country.")

    return pop_growth_percent


def age_structure(society, category):
    """
    returns the Age structure: division of population into age groups: 0-14, 15-24, 25-54, 55-64, 65 and over
    :param society: the society tag from the parsed country webpage
    :param category: a number indicating the age group
    :return: the age range details per chosen category
    """

    age_range_list = [0, 0, 0, 0, 0]  # we initialize the age range_list with zeros
    try:
        for index, age_group in enumerate(society.find(id='field-age-structure').contents[1:10:2]):
            match = re.findall(cfg.AGE_STRUCTURE_PATTERN, age_group.text)
            total_pop_percent = float(match[0][1])
            number_of_males = int(''.join(match[0][2].split(',')))
            number_of_females = int(''.join(match[0][3].split(',')))
            age_range_list[index] = {"total_percent": total_pop_percent, "num_of_males": number_of_males,
                                     "num_of_females": number_of_females}
    except AttributeError:
        logging.warning("Age structure parameters don't exist for this country.")

    return age_range_list[category]


def urbanization(society):
    """
    returns uban percent
    :param society: the society tag from the parsed country webpage
    :return: urbanization percent as type float
    """

    urban_percent = 0

    # Urbanization: The percent of urban growth in the country
    try:
        urban = society.find(id="field-urbanization")
        urban = urban.find(class_='subfield-number').text
        urban_percent = float(re.findall(cfg.URBAN_PATTERN, urban)[0])
    except AttributeError:
        logging.warning("Urbanization parameter doesn't exist for this country.")

    return urban_percent


def life_span(society, index):
    """
    returns tje life expectancy: total population, males within the population, females within the population
    :param society: the society tag from the parsed country webpage
    :param index: index indicating what lifespan data point to retrieve
    :return: the desired lifespan (total average, male, female)
    """
    lifespans = [0, 0, 0]
    try:
        for i, expectancy in enumerate(
                society.find(id="field-life-expectancy-at-birth").find_all(class_="subfield-number")):
            expectancy = expectancy.text
            temp = re.findall(cfg.EXPECTANCY_PATTERN, expectancy)
            lifespans[i] = float(temp[0])
        if not lifespans:
            lifespans = [0, 0, 0]
    except (IndexError, AttributeError):
        logging.warning("Life span parameter doesn't exist for this country.")

    return lifespans[index]


def coordinates(geography, lat_or_lon):
    """
    return the country's coordinates as a tuple
    :param geography: the geography tag from the parsed country webpage
    :param lat_or_lon: index indicating latitude or longitude
    :return: The coordinate pair (lat, lon) as type list
    """

    coordinate = [None, None]
    try:
        coord = geography.find(id="field-geographic-coordinates")
        coord = coord.find(class_="category_data subfield text").text
        coord1 = coord.strip().split(', ')
        for i, element in enumerate(coord1):
            if 'W' in element or 'S' in element:
                coordinate[i] = float(('-' + re.findall(cfg.COORD_PATTERN, element)[0]).strip().replace(' ', '.'))
            else:
                coordinate[i] = float(re.findall(cfg.COORD_PATTERN, element)[0].strip().replace(' ', '.'))
    except (UnboundLocalError, ValueError, AttributeError):
        logging.warning("Latitude and longitude could not be scraped for this country.")

    return coordinate[lat_or_lon]


def natural_resources(geography):
    """
    return dictionary with natural resources of the country split into categories
    :param geography: the geography tag from the parsed country webpage
    :return: a dictionary of the number on natural resources in each resource category
    """
    m, en, er, an, other = 0, 0, 0, 0, 0
    nat_resource = ""
    try:
        resources = geography.find(id="field-natural-resources")
        resources = resources.find(class_="category_data subfield text")
        nat_resource = resources.text.strip()
        # List of all of countries resources
        if re.search(',', nat_resource):
            nat_resource = nat_resource.split(', ')
        else:
            nat_resource = nat_resource.split()
        # For each resource, if it appears in one of the resource titles update the relevant variable
        for resource in nat_resource:
            m += resource.lower() in cfg.METALS
            en += resource.lower() in cfg.ENERGY_FUELS
            er += resource.lower() in cfg.NM_MINERALS
            an += resource.lower() in cfg.BIO
        other = len(nat_resource) - (m + en + er + an)
    except Exception as e:
        logging.warning(e)

    return {'metal_ores': m, 'fossil_fuels': en, 'non_metallic_minerals': er, 'biomass': an, 'other': other}


def climate(geographic):
    """
    return string with the country's climate
    :param geographic: the geography tag from the parsed country webpage
    :return: the type of climate in the country
    """

    clim = ""
    try:
        c_climate = geographic.find(id="field-climate")
        c_climate = c_climate.find(class_="category_data subfield text")
        c_climate = c_climate.text.strip()
        clim = re.split(cfg.SPLIT_PATTERN, c_climate)[0]
        if len(clim) > cfg.MAX_CLIMATE_STR:
            clim = clim[:cfg.MAX_CLIMATE_STR]
    except Exception as e:
        logging.warning(e)

    return clim


def coastline(geography):
    """
    return the Total coastline of a country
    :param geography: the parsed geography tag from the country webpage
    :return: the coastline in km as type float
    """
    coast_line = None
    try:
        for field_coastline in geography.find_all('div', id="field-coastline"):
            coastline_data = re.findall(cfg.COASTLINE_PATTERN, field_coastline.text)
            coast_line = float(''.join(coastline_data[0].split(',')))
    except (ValueError, IndexError):
        logging.warning('Coastline parameter cannot be scraped for this country.')

    return coast_line
